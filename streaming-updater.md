Streaming Updater One-Liners
===


## From the active deployment server:

- Backfill all configured wikis for the time period:

```
user@deploy1002:~$ cd /srv/deployment-charts/helmfile.d/services/cirrus-streaming-updater
python3 ./cirrus_reindex.py eqiad backfill <YYYY-mm-ddTHH:mm:ss> <YYYY-mm-ddTHH:mm:ss>
```

- Backfill specific wikis for a given time period:

```
user@deploy1002:~$ python3 ./srv/deployment-charts/helmfile.d/services/cirrus-streaming-updater/cirrus_reindex.py eqiad <wiki1> <wiki2> <...> backfill <YYYY-mm-ddTHH:mm:ss> <YYYY-mm-ddTHH:mm:ss>
```

- Reindex a single wiki

```
python3 ./cirrus_reindex.py eqiad testwiki reindex
```

- Reindex all wikis:
```
cluster=eqiad

 user@deploy1002:~$ cd /srv/deployment-charts/helmfile.d/services/cirrus-streaming-updater
 expanddblist all | while read wiki ; do
     python3 ./cirrus_reindex.py $cluster $wiki reindex | tee -a $HOME/cirrus_log/$wiki.$cluster.reindex.log
 done
 ```

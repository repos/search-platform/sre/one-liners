#!/usr/bin/env bash
# SPDX-License-Identifier: Apache-2.0
# a collection of helper functions for Elasticsearch operations

# declare -a
# curl -H 'Accept: application/yaml' -XGET -s http://localhost:${1}/_cluster/health?pretty

# health of the cluster: watch -d -n 5 curl -s -k 'https://localhost:9243/_cluster/health?pretty' (9243 is the port of the main cluster, replace with 9443 and 9643 to monitor the psi and omega clusters)
# unallocated shards: watch -d -n 30 "curl -s -k 'https://localhost:9243/_cat/shards' | grep -v STARTED | sort"
# ongoing recoveries: watch -d -n 5 "curl -s -k 'https://localhost:9243/_cat/recovery' | grep -v done | sort"
# number of nodes restarted more than a day ago: watch "curl -s -k 'https://localhost:9243/_cat/nodes?h=n,u' | sort | grep 'd\$' | wc -l"
# elasticsearch uptime on each node: watch "curl -s -k 'https://localhost:9243/_cat/nodes?h=n,u' | sort"
# shard allocation enabled (primaries/all): watch "curl -s -k 'https://localhost:9243/_cluster/settings?pretty'"

# bash function for Java debugging via jstack/JVM
systemctl-jstack() {
user_check=$(whoami)
if [ $user_check != 'root' ] 
then
   printf "%s\n" "Error, this script should be run as root"
   exit 1
fi
MAINPID=$(systemctl show $1 |grep "^MainPID" | awk -F= '{print $2}')
SVC_USER=$(systemctl show $1 |grep "^User" | awk -F= '{print $2}')
nsenter -t $MAINPID -m sudo -u $SVC_USER jstack $MAINPID
}

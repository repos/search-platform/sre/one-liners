# bash function for Java debugging via jstack/JVM
systemctl-jstack() {
user_check=$(whoami)
if [ $user_check != 'root' ] 
then
   printf "%s\n" "Error, this script should be run as root"
   exit 1
fi
MAINPID=$(systemctl show $1 |grep "^MainPID" | awk -F= '{print $2}')
SVC_USER=$(systemctl show $1 |grep "^User" | awk -F= '{print $2}')
nsenter -t $MAINPID -m sudo -u $SVC_USER jstack $MAINPID
}
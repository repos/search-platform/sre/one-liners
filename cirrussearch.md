CirrusSearch One-Liners
===

- Create index for new wiki
```
mwscript extensions/CirrusSearch/maintenance/UpdateSearchIndexConfig.php --wiki $wiki --cluster=all
```

- Populate the search index for new wiki

```
mkdir -p ~/log
clusters='eqiad codfw cloudelastic'
for cluster in eqiad codfw cloudelastic; do
    mwscript extensions/CirrusSearch/maintenance/ForceSearchIndex.php --wiki $wiki --cluster $cluster --skipLinks --indexOnSkip --queue | tee ~/log/$wiki.$cluster.parse.log
    mwscript extensions/CirrusSearch/maintenance/ForceSearchIndex.php --wiki $wiki --cluster $cluster --skipParse --queue | tee ~/log/$wiki.$cluster.links.log
done
```

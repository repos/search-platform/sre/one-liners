# One Liners

One-liners, bash functions, and other helpful snippets for managing
Search Platform Team-related infrastructure.

You can find the bash functions in the ".sh" files and one-liners in the Markdown files. 

## Contributing

Pull requests are always welcome! You don't have to be a member of Wikimedia Foundation to contribute.

### Questions?

Feel free to open an issue, or ask on libera.chat IRC in the #wikimedia-search room.

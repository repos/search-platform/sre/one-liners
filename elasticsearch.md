Elasticsearch One-Liners
===
Not sure when or why to use these calls? See the Search Platform docs.

- health of the cluster: `watch -d -n 5 curl -s 'http://localhost:9200/_cluster/health?pretty' (9200 is the port of the main cluster, replace with 9400 and 9600 to monitor the psi and omega clusters)`


- full dump of cluster state (LOTS of output!): `curl localhost:9200/_cluster/state?pretty | less`

- unallocated shards: `watch -d -n 30 "curl -s 'http://localhost:9200/_cat/shards' | grep -v STARTED | sort"`

- ongoing recoveries: `watch -d -n 5 "curl -s 'http://localhost:9200/_cat/recovery' | grep -v done | sort"`

- number of nodes restarted more than a day ago: `watch "curl -s 'http://localhost:9200/_cat/nodes?h=n,u' | sort | grep 'd\$' | wc -l"`

- elasticsearch uptime on each node: `watch "curl -s  'http://localhost:9200/_cat/nodes?h=n,u' | sort"`

- shard allocation enabled (primaries/all): `watch "curl -s 'http://localhost:9200/_cluster/settings?pretty'"`

Tell Elastic to use a shard it considers outdated:
```
curl -XPOST -H 'Content-Type: application/json' http://localhost:9200/_cluster/reroute -d'
{
   "commands": [
       {"allocate_stale_primary": {
           "index": "wikidatawiki_content_1692131793",
           "shard": 19,
           "accept_data_loss": true,
           "node": "cloudelastic1003-cloudelastic-chi-eqiad"
       }}
   ]}'
```

